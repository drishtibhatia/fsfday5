(function(){

//create instance of application with arg: app name and arg:agular object 
var CalculatorApp = angular.module("CalculatorApp",[]);

//function to add
var adding = function(a,b){
    return (a+b);
}
//function to multiply
var multiplying = function(a,b){
    return (a*b);
}
//function to multiply
var divideing = function(a,b){
    return (a/b);
}
//function to multiply
var subtracting = function(a,b){
    return (a-b);
}
var validation = function(a,b){
    var errorMessage ="";
    if(isNaN(a)||isNaN(b)){
     errorMessage = "Please enter number ";
    }
    return errorMessage;
}


//define fnction of our controller
var CalculatorCtrl = function(){
    //hold the referenc of object this.
    var calculatorCtrl = this;

    //define model {{calculatorCtrl.oper1}} for oper1
    calculatorCtrl.oper1 = "0";
    
    //define model {{calculatorCtrl.oper2}} for oper2
    calculatorCtrl.oper2 = "0";

    //define model answer  {{calculatorCtrl.answer}}
    calculatorCtrl.answer= "0";

    //define model for error {{calculatorCtrl.errorMessage}}
    calculatorCtrl.errorMessage ="";

    //model for adding on click event 
    calculatorCtrl.addOper1AndOper2 = function(){
        calculatorCtrl.errorMessage = validation(calculatorCtrl.oper1,calculatorCtrl.oper2);
        if(calculatorCtrl.errorMessage == ""){
            calculatorCtrl.answer = 
                adding(Number(calculatorCtrl.oper1),Number(calculatorCtrl.oper2)); 
        }
    }
    //model for multiplying on click event 
      calculatorCtrl.mulOper1AndOper2 = function(){
        calculatorCtrl.errorMessage = validation(calculatorCtrl.oper1,calculatorCtrl.oper2);
        if(calculatorCtrl.errorMessage == ""){
            calculatorCtrl.answer = 
                multiplying(Number(calculatorCtrl.oper1),Number(calculatorCtrl.oper2));
         } 
    }
       //model for divideing on click event 
      calculatorCtrl.divOper1AndOper2 = function(){
        calculatorCtrl.answer = "";
        calculatorCtrl.errorMessage = validation(calculatorCtrl.oper1,calculatorCtrl.oper2);
        if(calculatorCtrl.errorMessage == ""){
            if(calculatorCtrl.oper2 == 0){
                calculatorCtrl.errorMessage = "Oper 2 Can't Be Zero";
            }else{
                calculatorCtrl.answer = 
                    divideing(parseInt(calculatorCtrl.oper1),parseInt(calculatorCtrl.oper2));
                } 
            }
        }
        //model for divideing on click event
      calculatorCtrl.subOper1AndOper2 = function(){
        calculatorCtrl.answer = "";
        calculatorCtrl.errorMessage = validation(calculatorCtrl.oper1,calculatorCtrl.oper2);
        if(calculatorCtrl.errorMessage == ""){
            if(Number(calculatorCtrl.oper2) > Number(calculatorCtrl.oper1)){
                calculatorCtrl.errorMessage = "Oper 2 must be samller than Oper1";
            }else{
            calculatorCtrl.answer = 
                subtracting(Number(calculatorCtrl.oper1),Number(calculatorCtrl.oper2));
            } 
        }
    }
}
//calling the controller
CalculatorApp.controller("CalculatorCtrl",[CalculatorCtrl]);

})(); //() for executing the function 