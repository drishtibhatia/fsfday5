//for client side we use IIFE to execute the application 
//() for executing this 
(function(){
//everthign inside this function is our appliaction 
//create an instance of angular application 
var FirstApp = angular.module("FirstApp",[]);

//1st  arg is app's name 2nd arg is dependencies 
//'angular' is object provided by angular 

var reverse = function(s){
    return s.split("").reverse().join("");
}
//define fnction of our controller
var FirstCtrl = function(){
    //hold a refrence of this controller so tht when "this"chaneges we are still refrenceing the controller
    var firstCtrl = this;
    //define first model 
    //model <h1> {{firstCtrl.my.text}}</h1>   uses input <input ng-model="firstCtrl.my.text">
    firstCtrl.myText = "hello , world"; 

    //define other model
   firstCtrl.reverseMyText = "";

   //define an event for the click button 
   //<h1>{{firstCtrl.reverseMyText}}</h1> uses function reverseText() on click event <button > ng-click="firstCtrl.reverseText()"</button> 
   firstCtrl.reverseText = function(){
       firstCtrl.reverseMyText = reverse(firstCtrl.myText);
   }

//define an event for clear text
//<button ng-click="firstCtrl.clearText()" > on click clear the text  
firstCtrl.clearText = function(){
    firstCtrl.reverseMyText = "";
    }
  
}
//define controller 
FirstApp.controller("FirstCtrl",[FirstCtrl])
//1st arg is string (name of controller)FirstApp has FirstCtrl, 2nd arg is [FirstCtrl]use function FirstCtrl
//when agular says FirtCtrl is 1st arg , 2nd is just a function . 

})(); 
// if we dont put () at the end it will not excute 