//load the lib 
var path = require("path");
var express = require("express");

//create instance of app
var app = express();

app.use (express.static(__dirname+ "/public"));

app.use ("/libs",express.static(__dirname+ "/bower_components"));

//define the port that our app is going to listen to 
app.set("port",parseInt(process.argv[2])||3000);

//start the server 
app.listen(app.get("port"),function(){
console.log("application started at %s on %d"
            ,new Date(),app.get("port"));
});
